#include <iostream>
#include <fstream>
#include <string>
#include <vector>

int main()
{
	std::cout << "Advent of Code 2020 - Day 2 - Part 1\n";

	std::ifstream in ("input.txt", std::ifstream::in);
	std::vector<std::string> input;

	// Read
	while (!in.eof()) {
		std::string line;
		std::getline(in, line);
		if (!in.fail()) {
			input.push_back(line);
		}
	}

	std::vector<int> min, max;
	std::vector<std::string> chars, password;
	
	// Parse
	for (size_t i=0; i<input.size(); i++) {

		std::size_t dashpos = input[i].find("-");
		std::size_t spacepos = input[i].find(" ");

		min.push_back(std::stoi(input[i].substr(0, dashpos)));
		max.push_back(std::stoi(input[i].substr(dashpos+1, spacepos-dashpos)));
		chars.push_back(input[i].substr(spacepos+1, 1));
		password.push_back(input[i].substr(spacepos+4));
	}

	int wrongPasswords = 0;
	
	// Check
	for (size_t i=0; i<input.size(); i++) {
		
		int instances = 0;
		std::size_t pos = password[i].find(chars[i]);

		while (pos!=std::string::npos) {
			instances++;
			pos = password[i].find(chars[i], pos+1);
		}

		if (instances<min[i] || instances>max[i]) {
			wrongPasswords++;
		}
	}

	std::cout << "wrong passwords " << wrongPasswords << std::endl;
	std::cout << "correct passwords " << input.size() - wrongPasswords << std::endl;
}