#include <iostream>
#include <fstream>
#include <string>
#include <vector>

int main()
{
	std::cout << "Advent of Code 2020 - Day 2 - Part 2\n";

	std::ifstream in ("input.txt", std::ifstream::in);
	std::vector<std::string> input;

	// Read
	while (!in.eof()) {
		std::string line;
		std::getline(in, line);
		if (!in.fail()) {
			input.push_back(line);
		}
	}

	std::vector<int> firstpos, lastpos;
	std::vector<std::string> chars, password;
	
	// Parse
	for (size_t i=0; i<input.size(); i++) {

		std::size_t dashpos = input[i].find("-");
		std::size_t spacepos = input[i].find(" ");

		firstpos.push_back(std::stoi(input[i].substr(0, dashpos)));
		lastpos.push_back(std::stoi(input[i].substr(dashpos+1, spacepos-dashpos)));
		chars.push_back(input[i].substr(spacepos+1, 1));
		password.push_back(input[i].substr(spacepos+4));
	}

	int wrongPasswords = 0;
	
	// Check
	for (size_t i=0; i<input.size(); i++) {
		
		std::string firstchar = password[i].substr(firstpos[i]-1, 1);
		std::string lastchar = password[i].substr(lastpos[i]-1, 1);

		if ((chars[i]==firstchar && chars[i]==lastchar) || (chars[i]!=firstchar && chars[i]!=lastchar)) {
			wrongPasswords++;
			std::cout << i << std::endl;
		}
	}

	std::cout << "wrong passwords " << wrongPasswords << std::endl;
	std::cout << "correct passwords " << input.size() - wrongPasswords << std::endl;
}