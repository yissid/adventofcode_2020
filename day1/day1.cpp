#include <iostream>
#include <fstream>
#include <string>
#include <vector>

int main()
{
	std::cout << "Advent of Code 2020 - Day 1 - Part 1\n";

	std::ifstream in ("input.txt", std::ifstream::in);
	std::vector<int> input;

	while (!in.eof()) {
		std::string line;
		std::getline(in, line);
		if (!in.fail()) {
			input.push_back(std::stoi(line));
		}
	}

	for (size_t i=0; i<input.size(); i++) {

		int dif = 2020-input[i];

		for (size_t j=i+1; j<input.size(); j++) {
			
			if (dif == input[j]) {
				std::cout << "entry " << i << " = " << input[i] << " and entry " << j << " = " << input[j] << std::endl;
				std::cout << input[i] << " x " << input[j] << " = " << input[i] * input[j] << std::endl;
			}
		}
	}

}