#include <iostream>
#include <fstream>
#include <string>
#include <vector>

int main()
{
	std::cout << "Advent of Code 2020 - Day 1 - Part 2\n";

	std::ifstream in("input.txt", std::ifstream::in);
	std::vector<int> input;

	while (!in.eof()) {
		std::string line;
		std::getline(in, line);
		if (!in.fail()) {
			input.push_back(std::stoi(line));
		}
	}

	for (size_t i=0; i<input.size(); i++) {

		for (size_t j=i+1; j<input.size(); j++) {

			int dif = 2020 - input[i] - input[j];

			for (size_t k=j+1; k<input.size(); k++) {

				if (dif == input[k]) {
					std::cout << "entry " << i << " = " << input[i] << std::endl;
					std::cout << "entry " << j << " = " << input[j] << std::endl;
					std::cout << "entry " << k << " = " << input[k] << std::endl;
					std::cout << input[i] << " x " << input[j] << " x " << input[k] << " = " << input[i]*input[j]*input[k] << std::endl;
				}
			}
		}
	}

}